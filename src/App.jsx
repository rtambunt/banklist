import Customer from "./features/customer/Customer";
import Balance from "./features/account/Balance";
import AccountOperations from "./features/account/AccountOperations";

function App() {
  return (
    <div>
      <h1>BankList</h1>

      <Customer />
      <Balance />
      <AccountOperations />
    </div>
  );
}

export default App;
