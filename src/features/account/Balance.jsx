import { useSelector } from "react-redux";

function Balance() {
  const balance = useSelector((store) => store.account.balance);

  return <h2>${balance}</h2>;
}

export default Balance;
