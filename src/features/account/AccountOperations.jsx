import { useDispatch, useSelector } from "react-redux";

import { deposit, withdraw, requestLoan, payLoan } from "./accountSlice";
import { useState } from "react";

function AccountOperations() {
  const [depositAmount, setDepositAmount] = useState(0);
  const [withdrawAmount, setWithdrawAmount] = useState(0);
  const [currency, setCurrency] = useState("usd");
  const [loanAmount, setLoanAmount] = useState(0);
  const [loanReason, setLoanReason] = useState("");

  const { loan: currentLoan, loanPurpose: currentLoanReason } = useSelector(
    (store) => store.account
  );

  const dispatch = useDispatch();

  //   Deposit Event Handler
  function handleDeposit() {
    if (!depositAmount || depositAmount <= 0) {
      setDepositAmount(0);
      return;
    }

    dispatch(deposit(depositAmount));
    setDepositAmount(0);
  }

  // Withdraw Event Handler
  function handleWithdraw() {
    if (!withdrawAmount || withdrawAmount <= 0) {
      setWithdrawAmount(0);
      return;
    }

    dispatch(withdraw(withdrawAmount));
    setWithdrawAmount(0);
  }

  // Loan Request Event Handler
  function handleLoanRequest() {
    if (!loanAmount) {
      alert("Please enter a loan amount");
      return;
    } else if (!loanReason) {
      alert("Please enter a loan reason");
      return;
    }

    dispatch(requestLoan(loanAmount, loanReason));
    setLoanAmount(0);
    setLoanReason("");
  }

  function handlePayLoan() {
    dispatch(payLoan());
  }

  return (
    <div>
      <h2>Account Operations</h2>
      <div>
        <div>
          <label htmlFor="deposit">Deposit</label>
          <input
            type="number"
            min={0}
            id="deposit"
            value={depositAmount}
            onChange={(e) => setDepositAmount(+e.target.value)}
          />
          <select
            value={currency}
            onChange={(e) => setCurrency(e.target.value)}
          >
            <option value="">Choose Currency Type</option>
            <option value="usd">USD</option>
            <option value="can">CAN</option>
            <option value="jpy">JPY</option>
            <option value="eur">EUR</option>
          </select>
          <button onClick={handleDeposit}>Deposit</button>
        </div>

        <div>
          <label htmlFor="withdraw">Withdraw</label>
          <input
            type="number"
            min={0}
            id="withdraw"
            value={withdrawAmount}
            onChange={(e) => setWithdrawAmount(+e.target.value)}
          />
          <button onClick={handleWithdraw}>Withdraw</button>
        </div>

        <div>
          <label htmlFor="loan"></label>
          <input
            type="number"
            min={0}
            id="loan"
            onChange={(e) => setLoanAmount(+e.target.value)}
            value={loanAmount}
          />
          <input
            type="text"
            placeholder="Reason for Loan"
            onChange={(e) => setLoanReason(e.target.value)}
            value={loanReason}
          />
          <button onClick={handleLoanRequest}>Request Loan</button>
        </div>
        {currentLoan > 0 && (
          <div>
            <p>Current Loan: {currentLoan}</p>
            <p>Loan Reason: {currentLoanReason}</p>
            <button onClick={handlePayLoan}>Pay Loan</button>
          </div>
        )}
      </div>
    </div>
  );
}

export default AccountOperations;
